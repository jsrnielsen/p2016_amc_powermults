# Contains utilities for maniulating the simulation files

import os, os.path
import re

base_dir = "power_sim"



def get_params_from_dir_name(name):
    def num(e):
        return r"(?P<" + e + r">\d*)"
    letters = ["q","n","k","t","s","l"]
    rexp = "_".join([e + num(e) for e in letters])
    m = re.match(rexp, name)
    if m is None:
        raise ValueError("Name is not valid: '"+ name + "'")
    def letter_to_id(e):
        if e == "t":
            return "tau"
        if e == "l":
            return "ell"
        else:
            return e
    return { letter_to_id(e): m.group(e) for e in letters }


def get_dir_name(p):
    return "%s/q%s_n%s_k%s_t%s_s%s_l%s" % (base_dir, p['q'],p['n'],p['k'],p['tau'],p['s'],p['ell'])


def simulations():
    """Return the parameters of all simulations performed, determined by looking
    at which folders are present."""
    entries = os.listdir(base_dir)
    all_params = set()
    for e in entries:
        try:
            all_params.add(frozenset(get_params_from_dir_name(e).items()))
        except ValueError:
            print "Malformed directory/file name: ", e
            continue
    return all_params


def simulation_result(params, debug=False):
    """Return a triple (N,F,W) representing the simulation performed for the
    given parameters: N is the number of simulations, E the number failures (no
    decoding) and W the number of wrong decodings."""
    dir_name = get_dir_name(params)
    if not os.path.exists(dir_name):
        return 0,0,0
    files = os.listdir(dir_name)
    count = 0
    failed = 0
    wrong = 0
    lineno = 0
    for f in files:
        handle = open(dir_name + '/' + f,'r')
        lineno = 0
        for line in handle:
            lineno += 1
            if line.strip() == '':
                continue
            try:
                ind = line.rindex('\t')
            except ValueError:
                if debug: print "Malformed line %s in file %s\n\t'%s'" % (lineno, dir_name + '/' + f, line)
                break
            res = line[ind+1:-1]
            if res == 'C':
                count += 1
            elif res == 'F':
                failed += 1
                count += 1
            elif res == 'W':
                wrong += 1
                count += 1
            else:
                if debug: print "Malformed line %s in file %s\n\t'%s'" % (lineno, dir_name + '/' + f, line)
                break
    return (count, failed, wrong)
