## Script for testing the failure rate of Power decoding with multiplicities
## Usage
##    ./sage failure_rate.sage q n k s ell tau N
## tests an [n,k] RS code over GF(q) with Power decoding (s,ell)
## with N random error patterns of weight exactly tau.
## 
## Uses the lemma from the paper which states that Power decoding fails only if
## there exist solutions to a set of congruences mod Lambda^s.
## Since that lemma is "only if" this script overestimates the true failure
## probability.
##
## Written by Johan S. R. Nielsen, 2015
##
## All the meat of the codes and decoders is implemented in the separate library
## Codinglib (see below), which this file calls. This file demonstrates the
## calling convention, as well as contain code for running the simulation
## described in the Simulations section of the paper.
##
## This implementation is in Sage (v. 7.1, http://sagemath.org). It depends on
## Codinglib by Johan S. R. Nielsen (http://jsrn.dk/codinglib). This
## sheet was tested and verified using the commit cc7375 of Codinglib.
##
## Please read the README of the code repository for a general description of
## the sheet format and usage instructions. Be aware that this is a
## simple research implementation. It is not well documented and
## probably not fully generic.

## LICENSE INFORMATION:
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.




import sys
old_all = all
from codinglib import *
all = old_all

if len(sys.argv) != 8:
    print "Usage:\n\t./sage failure_rate.sage q n k tau s ell N"
    sys.exit()

q   = int(sys.argv[1])
n   = int(sys.argv[2])
k   = int(sys.argv[3])
tau = int(sys.argv[4])
s   = int(sys.argv[5])
ell = int(sys.argv[6])
N   = int(sys.argv[7])

F = GF(q,'a')
C = GRS(F, n, k, F.list()[:n])
###

def is_error_decodable(e, C, (s,ell)):
    """Check whether the error is decodable by verifying the output of the decoder.
    For the fireworks, we add a random codeword before attempting decoding,
    though this has been proved to not change the failure behaviour.
    """
    c = C.random_codeword() 
    answer = None
    try:
        cout = C.Dec.decode(e+c)
        if cout == c:
            answer = "C"
        else:
            answer = "W"
    except DecodingFailedError:
        answer = "F"
    return answer

def prepare_code(C,tau,(s,ell)):
    "Precompute some values and set as fields for C"
    C.Dec = GRSDecoderPower(C, (s,ell))

###
prepare_code(C,tau,(s,ell))

string_buffer = ""
for iter in range(N):
    e = random_error(n, F, tau)
    string_buffer += "%s\t%s\n" % (e, is_error_decodable(e, C, (s,ell)))
    if (iter % 100) == 0:
        print string_buffer
        sys.stdout.flush()
        string_buffer = ""
print string_buffer
sys.stdout.flush()
