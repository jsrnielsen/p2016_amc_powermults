# Prints the simulation results
##
## Written by Johan S. R. Nielsen, 2015
##
## All the meat of the codes and decoders is implemented in the separate library
## Codinglib (see below), which this file calls. This file demonstrates the
## calling convention, as well as contain code for running the simulation
## described in the Simulations section of the paper.
##
## This implementation is in Sage (v. 7.1, http://sagemath.org). It depends on
## Codinglib by Johan S. R. Nielsen (http://jsrn.dk/codinglib). This
## sheet was tested and verified using the commit cc7375 of Codinglib.
##
## Please read the README of the code repository for a general description of
## the sheet format and usage instructions. Be aware that this is a
## simple research implementation. It is not well documented and
## probably not fully generic.

## LICENSE INFORMATION:
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.





load("util.sage")
from codinglib.rs_power import power_decoding_radius_from_parameters_real
from codinglib.util import result_table


#Make a new dictionary <code&decoder-params> -> decoding radius -> simulation-results
def read_results():
    all_params = simulations()

    results = dict()

    def add_results(key, tau, sim_res):
        fzkey = frozenset(key.items())
        if fzkey not in results:
            results[fzkey] = dict()
        d = results[fzkey]
        d[tau] = sim_res

    for p in all_params:
        p = dict(p)
        sim_results = simulation_result(p, debug=False)
        reskey = { e: p[e] for e in p if e != 'tau' }
        add_results(reskey, p['tau'], sim_results)

    return results

def print_ascii():
    results = read_results()
    for p in results:
        dp = dict(p)
        print "Code: [%s, %s] over GF(%s). (s,ell) = (%s, %s)" % (dp['n'],dp['k'],dp['q'],dp['s'],dp['ell'])
        titles = [ "tau", "Trials", "N(fail)", "P(fail)", "N(wrong)", "P(wrong)", "P(not-recov)" ]
        types = [ "int", "int", "int", "exp", "int", "exp", "exp" ]
        resd = dict()
        for tau_s in results[p].keys():
            (N,fail,wrong) = results[p][tau_s]
            resd[int(tau_s)] = [N, fail, 1.*fail/N, wrong, 1.*wrong/N, 1.*(wrong+fail)/N]
        result_table(titles, resd, types, col_width=10, exp_precision=3)
        print

def print_latex():
    results = read_results()
    for p in results:
        res = results[p]
        dp = dict(p)
        n,k,q,s,ell = (int(dp[t]) for t in ['n','k','q','s','ell'])
        code_str = "[%s, %s]_{\\GF{%s}}" % (n,k,q)
        param_str = "(%s,%s)" % (s,ell)
        tauPow = power_decoding_radius_from_parameters_real(n,k,(s,ell))
        if tauPow == floor(tauPow):
            tau_str = str(tauPow)
        else:
            tau_str = "%d %s" % (floor(tauPow), latex(tauPow - floor(tauPow)))
        taus = [ int(t) for t in res.keys() ]
        taus.sort()
        taus_str = []
        myRR = RealField(30, sci_not=True) #Always display scientific notation
        allN = None
        for tau in taus:
            (N,fail,wrong) = res[str(tau)]
            if wrong+fail == 0:
                taus_str.append("0")
            elif wrong+fail == 1:
                taus_str.append("1")
            else:
                taus_str.append( latex(myRR((wrong+fail)/N)) )
            allN = N #TODO: Make check?
        N_str = str(allN)
        maths = [ '$'+string+'$' for string in [code_str, param_str, tau_str] + taus_str + [N_str]]
        print "\t&\t".join(maths) + "\t\\\\"

###

import sys
if len(sys.argv) > 1 and sys.argv[1] == "latex":
    print_latex()
else:
    print_ascii()
