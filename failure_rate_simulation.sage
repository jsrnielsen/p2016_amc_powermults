## Script for orchestrating a simulation of failure rates for many codes and
## parameters.
##
## The simulations are individually carried out by failure_rate_powermults.sage.
## 
## We distribute the computation over a number of machines using SSH, and we save
## the results in simple clear-text files. This script is intended to be run
## within screen or similar, and lives throughout the entire simulation (across
## all machines).
##
## The results are saved by each machine individually in files, in a subdirectory
## sims. It is assumed that all machines have this directory as a sub-directory
## of their home-dir (e.g. achieved by all machines sharing the same home-dir).
##
##
## Written by Johan S. R. Nielsen, 2015
##
## All the meat of the codes and decoders is implemented in the separate library
## Codinglib (see below), which this file calls. This file demonstrates the
## calling convention, as well as contain code for running the simulation
## described in the Simulations section of the paper.
##
## This implementation is in Sage (v. 7.1, http://sagemath.org). It depends on
## Codinglib by Johan S. R. Nielsen (http://jsrn.dk/codinglib). This
## sheet was tested and verified using the commit cc7375 of Codinglib.
##
## Please read the README of the code repository for a general description of
## the sheet format and usage instructions. Be aware that this is a
## simple research implementation. It is not well documented and
## probably not fully generic.

## LICENSE INFORMATION:
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.






LOCAL_TEST = False

from codinglib.parallel import *
from codinglib.util import flatten_once
from codinglib.rs import GRS
from codinglib.rs_power import GRSDecoderPower
import time

load("util.sage")

machines = [ "coeos.grace", "cronos.grace", 
             "japet.grace", "ocean.grace", "phebe.grace", "rhea.grace", 
             "theia.grace", "typhon.grace", "prometheus.grace", 
             "epimetheus.grace", "menoetius.grace", "uranus.grace", "astraeus.grace", ]
# Titans which are off bounds:
# "crios.grace", "mnemosyne.grace", "themis.grace", "hyperion.grace",
threads_per_machine = 14

sims_small = 10^6
sims_big = 10^5
chunk_count = 2*10^2

elapsed_threshold = 10.0

if LOCAL_TEST:
    threads_per_machine = 1
    sims_small = 10^1
    sims_big = 2*10^0
    chunk_count = 1
    elapsed_threshold = 0

# Goal. These are (q, n, k, (s,ell)) tuples.
goals = \
    [ (tup, sims_small) for tup in 
    # Small codes -> many simulations     
        (25, 24,  7, (2,3))
      , (37, 32, 10, (2,4))
      , (71, 68, 31, (3,4))
      , (23, 21,  3, (6,19))
      ]
    ] + \
    [ (tup, sims_big) for tup in
    # Big codes -> fewer simulations
      [ (64, 64, 27, (2,3))
      , (125,125, 51, (4,6))
      , (256,256, 63, (2,4))
      ]
    ]



class MyWorkerSSH(Worker):
    def __init__(self, host):
        Worker.__init__(self)
        self.host = host
        if LOCAL_TEST:
            self.host = "LOCAL TEST"

    def perform(self, task):
        def task_wrap():
            print "STARTING task on host %s:\n\t%s" % (self.host, task.cmd)
            done = False
            count = 1
            begin_total = time.time()
            while not done:
                before = time.time()
                if LOCAL_TEST:
                    ssh_task = TaskShellCommand(task.cmd)
                else:
                    ssh_task = TaskShellCommand("ssh "+ self.host + " \"" + task.cmd + "\"")
                ssh_task()
                elapsed = time.time() - before
                if elapsed < elapsed_threshold:
                    print "\t\tTASK REFUSED on %s! Rerunning in five minutes (%s)" % (self.host, count)
                    sleep(5*60)
                    count += 1
                else:
                    done = True
            elapsed_total = time.time() - begin_total
            print "COMPLETED on host %s. Took %s secs, total of %s secs.\n\t%s" % (self.host, elapsed, elapsed_total, task.cmd)
        Worker.perform(self, task_wrap)

def build_worker_pool():
    workers = WorkerPool()
    for machine in machines:
        for n in range(threads_per_machine):
            workers.add(MyWorkerSSH(machine))
    return workers

def get_id():
    return randint(1, 10^12)

def get_decoding_radius(p):
    F = GF(p['q'],'a')
    C = GRS(F,p['n'],p['k'],F.list()[:p['n']])
    return GRSDecoderPower.decoding_radius_from_parameters(C, (p['s'], p['ell']))

def build_tasks():
    tasks = []
    for p in codes_and_parameters:
        count = codes_and_parameters[p]
        chunk = chunk_size[p]
        if count <= 0:
            continue
        p = dict(p) #p was a frozenset for hashing purposes
        dir_name = get_dir_name(p)
        mkdir = "mkdir -p "+ dir_name
        sage = "./sage" if not LOCAL_TEST else "sage"
        script = sage + " failure_rate_powermults.sage %s %s %s %s %s %s" % (p['q'],p['n'],p['k'],p['tau'],p['s'],p['ell'])
        while count > 0:
            id = get_id()
            N = min(count, chunk)
            cmd = "%s; %s %s &> %s/%s.res" % (mkdir, script, N, dir_name, id)
            #cmd = "%s > %s/%s.res" % (script, dir_name, id)
            tasks.append(TaskShellCommand(cmd))
            count -= N
    return tasks

def code_param_dict(q,n,k,(s,ell)):
    return {'q': q, 'n': n, 'k': k, 's': s, 'ell': ell}

def create_code_params_dict():
    g = dict()
    for (ptup, count) in goals:
        params = code_param_dict(*ptup)
        dec_rad = get_decoding_radius(params)
        for tau in range(dec_rad-1,dec_rad+2):
            params['tau'] = tau
            g[frozenset(params.items())] = count
    return g



###########
# Let's go!

            
codes_and_parameters = create_code_params_dict()
chunk_size = dict()
# Update the goals by reducing count-goals with work already done
for p in codes_and_parameters:
    chunk_size[p] = ceil(codes_and_parameters[p]/chunk_count)
    (already_done,_,_) = simulation_result(dict(p))
    print p , codes_and_parameters[p] , already_done
    codes_and_parameters[p] -= already_done

workers = build_worker_pool()
manager = Manager(workers)
tasks = build_tasks()
shuffle(tasks)

print "%s different code parameters given." % len(codes_and_parameters)
print "%s threads in total, distributed on %s machines" % (workers.count(), len(machines))
print "Performing a total of %s simulations in %s tasks" % (sum(codes_and_parameters[p] for p in codes_and_parameters), len(tasks))

manager.perform_tasks(tasks)

print "All done"
