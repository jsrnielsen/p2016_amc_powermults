CODINGLIB=$(HOME)/mat/codinglib

.PHONY: update
update: failure_rate_simulation.sage failure_rate_powermults.sage util.sage simulation_results.sage
	scp failure_rate_simulation.sage failure_rate_powermults.sage util.sage simulation_results.sage coeos.grace:.

.PHONY: codinglib
codinglib:
	scp $(CODINGLIB)/*.py coeos.grace:codinglib/
