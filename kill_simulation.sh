#!/bin/bash
for t in coeos crios cronos hyperion japet mnemosyne ocean phebe rhea theia themis prometheus epimetheus menoetius uranus astraeus; do
    echo "Killing at $t"
    ssh $t.grace killall -u jnielsen python
done
