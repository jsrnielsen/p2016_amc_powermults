#!/bin/bash
#From the simulation results, keep only the surprising ones (failures when
#expecting correct, correct when expecting failures)
export SIMDIR=./power_sim
export INTDIR=power_sim_int

export KEEP_FAIL="q125_n125_k51_t41_s4_l6 q125_n125_k51_t42_s4_l6 q23_n22_k3_t13_s6_l18 q23_n22_k3_t14_s6_l18 q256_n256_k63_t115_s2_l4 q256_n256_k63_t116_s2_l4 q25_n24_k7_t9_s2_l4 q25_n24_k7_t10_s2_l4 q32_n32_k9_t12_s2_l3 q32_n32_k9_t13_s2_l3 q64_n64_k29_t18_s4_l5 q64_n64_k29_t19_s4_l5 q71_n68_k31_t19_s3_l4 q71_n68_k31_t20_s3_l4"

export KEEP_SUCC="q125_n125_k51_t43_s4_l6 q23_n22_k3_t15_s6_l18 q256_n256_k63_t117_s2_l4 q25_n24_k7_t11_s2_l4 q32_n32_k9_t14_s2_l3 q64_n64_k29_t20_s4_l5 q71_n68_k31_t21_s3_l4" 

echo "Keep fails"
for d in $KEEP_FAIL; do
    echo $d
    grep '[FW]' $SIMDIR/$d/* > $INTDIR/$d.res
done

echo
echo "Keep successes"
for d in $KEEP_SUCC; do
    echo $d
    grep 'C' $SIMDIR/$d/* > $INTDIR/$d.res
done
